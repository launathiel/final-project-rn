import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image, Input, TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import { color } from 'react-native-reanimated';
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";


export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    this.props.navigation.navigate('LoginScreen');
  }

  render() {
    return (
      <View style={styles.container}>
        <View style ={{paddingTop:50,}}>
          <View style = {{alignSelf: 'center', justifyContent: 'center', marginLeft: 60}}>
              <Image source={require('./assets/gambar.png')} style={{width:200, height:100}}/>
          </View>
        </View>
        <View style={{marginBottom : 50,}}>
          <Text style={styles.logintext}>Sign Up</Text>
        </View>
        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Email</Text>
              <View style ={{flexDirection: 'row', marginTop: 4}}>
                <MaterialCommunityIcons name='email' color='white' size={35} />
                <TextInput
                  style={styles.textInput}
                  placeholder='Enter Your Email Here'
                  onChangeText={userName => this.setState({ userName })}
                />
              </View>
            </View>
          </View>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Username</Text>
              <View style ={{flexDirection: 'row', marginTop: 4}}>
                <MaterialCommunityIcons name='account' color='white' size={35} />
                <TextInput
                  style={styles.textInput}
                  placeholder='Enter Your Username Here'
                  onChangeText={userName => this.setState({ userName })}
                />
              </View>
            </View>
          </View>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Password</Text>
              <View style ={{flexDirection: 'row', marginTop: 4}}>
                <MaterialCommunityIcons name='lock' color='white' size={35} />
                <TextInput
                  style={styles.textInput}
                  placeholder='Enter Your Password Here'
                  onChangeText={password => this.setState({ password })}
                  secureTextEntry={true}
                />
              </View>
            </View>
          </View>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Reenter Password</Text>
              <View style ={{flexDirection: 'row', marginTop: 4}}>
                <MaterialCommunityIcons name='lock' color='white' size={35} />
                <TextInput
                  style={styles.textInput}
                  placeholder='Enter Your Password Again'
                  onChangeText={password => this.setState({ password })}
                  secureTextEntry={true}
                />
              </View>
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Login Failed!</Text>
          <View style ={{marginBottom:30}}>
            <Button title='Sign Up' onPress={() => this.loginHandler()} />
          </View>
          <View style={{alignContent: 'center', justifyContent: 'center', marginLeft: 98,}}>
            <TouchableOpacity style={styles.btreg}
              onPress={ () => this.loginHandler() } >
              <Text style={styles.textbt}> Back </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#274472',
  },
  logintext: {
    fontSize: 30,
    color: 'white',
    textAlign: "center",
    marginVertical: 25
    },
  subTitleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'flex-end',
    marginBottom: 25,
  },
  formContainer: {
    justifyContent: 'center',
    flex : 1,
    marginTop: -100,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold',
    color: 'white',
  },
  textInput: {
    width: 250,
    backgroundColor: '#274472',
    marginLeft : 10,
    borderRadius: 8,
    color: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }, 
  btreg: {
    alignItems: "center",
    backgroundColor: "#c3e0e5",
    textDecorationColor: 'white',
    padding: 8,
    borderRadius: 12,
    marginTop: -10,
    width: 100
},
});
