import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Axios from 'axios';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://jobs.github.com/positions.json?result=10`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={styles.LogoBar}>
          <Image source={require('./assets/gambar.png')} style={{width:150, height:80}}/>
          <Text style ={{color: 'white', fontSize: 20}}>My Employee</Text>
        </View>
        <View style={styles.boxKategori}>
          <View style={{marginTop: 10}}>
            <Text style={styles.PalingAtas}>Job Available</Text>
            <View style={{ borderBottomColor: '#003366', borderBottomWidth: 2, padding:1, borderBottomEndRadius:30, marginBottom:5}}/>
          </View>
          <FlatList
            data={this.state.data}
            renderItem={({ item }) =>
              <View style={styles.viewList}>
                <View style = {{marginLeft:-20}}>
                  <Image source={{ uri: `${item.company_logo}` }} style={styles.Image} />
                </View>
                <View style={{width:220}}>
                  <Text style={styles.textItemLogin}>{item.title}</Text>
                  <Text style={styles.textItemUrl}> {item.type}</Text>
                  <Text style={styles.textItemUrl}> {item.location}</Text>
                </View>
              </View>
            }
            keyExtractor={({ id }, index) => index}
            ItemSeparatorComponent={()=><View style={{height: 0.5, backgroundColor: '#cccccc'}}/>}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#274472',
    alignItems: 'center',
  },
  LogoBar:{
    marginTop:14,
    marginLeft:36,
    marginBottom:50,
  },
  PalingAtas: {
    fontSize: 28,
    paddingBottom: 4,
    color:'white',
    fontWeight: 'bold',
  },
  boxKategori: {
    flex:2,
    backgroundColor: '#5885af',
    width:340,
    marginBottom: -40,
    borderRadius: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  viewList: {
    flex: 1,
    height: 150,
    width:370,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 20
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 15,
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 14,
    marginTop: 10,
    color: 'blue'
  }
})
