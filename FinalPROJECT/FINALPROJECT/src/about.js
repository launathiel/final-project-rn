import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text ,View ,Image, TouchableOpacity, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function App() {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.PalingAtas}>About Us</Text>
      </View>
        <View style={styles.Photo}>
          <Image source={require('./assets/gambar.png')} style={{width:180, height:110}}/>
        </View>
      <View style={styles.Biodata}>
        <Text style={styles.IsiBiodata1}>My Employee</Text>
        <Text style={styles.IsiBiodata2}>Get Job Anytime, anywhere.</Text>
      </View>
      <View style={styles.boxx}>
        <View style={styles.box69}>
          <TouchableOpacity style={styles.tabItem1}>
            <View style={{marginBottom: 10}}>
              <Icon style={styles.navItem} name="map-marker" size={50} color='white'></Icon>
            </View>
            <Text style ={{color: 'white',}}>Find Us!</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem1}>
            <View style={{marginBottom: 10}}>
              <Icon style={styles.navItem} name="firefox" size={50} color='white'></Icon>
            </View>
            <Text style ={{color: 'white',}}>Visit Us!</Text>
          </TouchableOpacity>
        </View>  
        <View style={styles.box2}>
          <Text style={styles.textBox}>Contact Us</Text>
          <View style={{ borderBottomColor: '#003366', borderBottomWidth: 1, padding:2}}/>
            <View style={styles.iconBox1}>
              <TouchableOpacity style={styles.tabItem1}>
                <Icon style={styles.navItem} name="linkedin" size={32} color='#274472'></Icon>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem1}>
                <Icon style={styles.navItem} name="twitter" size={32} color='#274472'></Icon>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem1}>
                <Icon style={styles.navItem} name="instagram" size={32} color='#274472'></Icon>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem1}>
                <Icon style={styles.navItem} name="facebook" size={32} color='#274472'></Icon>
              </TouchableOpacity>
            </View>
          </View>  
        </View>      
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        backgroundColor: '#274472',
        alignItems: 'center',
        justifyContent: 'center',

    },
    PalingAtas: {
        fontSize: 40,
        marginTop:10,
        paddingBottom: 8,
        color:'white',
        fontWeight: 'bold',
    },
    Photo: {
        marginTop:10,
        marginBottom:40,
    },
    boxx: {
        margin:2,
    },
    box1: {
        width:294,
        height:150,
        padding: 4,
        backgroundColor: '#c3e0e5',
        margin: 15,
        borderRadius:10,
    },
    box2: {
        width:294,
        height:100,
        padding: 5,
        backgroundColor: '#c3e0e5',
        margin: 15,
        borderRadius:10,
    },
    box69: {
      width:294,
      height:100,
      padding: 5,
      margin: 15,
      borderRadius:10,
      flexDirection: 'row',
      justifyContent: 'space-around'
      
    },
    Biodata: {
        marginBottom: 6,
    },
    IsiBiodata1:{
        fontSize: 30,
        paddingBottom: 8,
        color:'white',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    IsiBiodata2:{
        fontSize: 16,
        paddingBottom: 8,
        color:'#dbf5f0',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    iconBox1: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 10,
    },
    iconBox2: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 10,
    },
    textBox: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#003366'
    },
    navItem: {
        margin: 2,
        alignItems: 'center',
    },
    tabItem1: {
        alignItems:'center',
        justifyContent: 'center',
      },
    tabItem2: {
        paddingLeft:60,
        alignItems:'flex-start',
        justifyContent: 'center',
      },
    contact: {
        fontSize: 15,
        textAlign: 'center',

    }
})