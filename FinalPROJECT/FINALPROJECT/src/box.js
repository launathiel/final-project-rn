import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, Platform, TouchableOpacity, Button} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/FontAwesome5'
import { setStatusBarNetworkActivityIndicatorVisible } from 'expo-status-bar';

export default class box extends Component {
 render() {
    let box = this.props.box;
    return(
        <View style={styles.container}>
            <View style ={styles.body}>
                <View style = {styles.box1}>
                    <View style= {{flexDirection:'row', justifyContent:'space-around', paddingTop:20}}>
                        <MaterialCommunityIcons name={box.iconName} size={60} />    
                        <View style= {{flexDirection:'column', justifyContent: 'space-evenly', marginTop:-10}}>
                            <Text style={{fontSize:28, textAlign:"center"}}>{box.skillName}</Text>
                            <Text style={{fontSize:15, textAlign:"center"}}>{box.categoryName}</Text>
                        </View>
                        <TouchableOpacity>
                            <MaterialCommunityIcons name="compress" size={30} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
    },
    box1: {
        width:320,
        height:110,
        padding: 4,
        backgroundColor: '#5885af',
        margin: 4,
        borderRadius:8,
    },
    body: {
        margin:-15
    }
});
