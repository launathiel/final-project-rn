import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text ,View ,Image, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import data from './skillData.json';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import BoxItem from './box';

export default function App() {

  return (
    <View style={styles.container}>
      <View style={styles.LogoBar}>
        <Image source={require('./assets/gambar.png')} style={{width:150, height:80}}/>
        <Text style ={{color: 'white', fontSize: 20}}>My Employee</Text>
      </View>
      <View style={styles.boxKategori}>
        <View style={{marginTop: 15}}>
          <Text style={styles.PalingAtas}>Kind of Job</Text>
          <View style={{ borderBottomColor: '#003366', borderBottomWidth: 2, padding:1, borderBottomEndRadius:30,}}/>
        </View>
        <View style={styles.body}>
            <FlatList
            data={data.items}
            renderItem={(box)=><BoxItem box={box.item}/>}
            keyExtractor={(item)=>item.id}
            />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#274472',
        alignItems: 'center',
    },
    LogoBar:{
        marginTop:14,
        marginLeft:36,
        marginBottom:50,
    },
    PalingAtas: {
        fontSize: 28,
        paddingBottom: 4,
        color:'white',
        fontWeight: 'bold',
    },
    boxKategori: {
        flex:1,
        backgroundColor: '#5885af',
        height: 2000,
        marginBottom: -30,
        borderRadius: 40,
        alignItems:'center',
        justifyContent:'center',
    },
    welcome: {
      color: 'white',
      fontSize: 20,
    },
    body:{
        flex:1,
        marginBottom:8,
    },
    
    box2: {
        width:294,
        height:100,
        padding: 5,
        backgroundColor: '#EFEFEF',
        margin: 15,
        borderRadius:10,
    },
    Biodata: {
        marginBottom: 6,
    },
    IsiBiodata1:{
        fontSize: 24,
        paddingBottom: 8,
        color:'#003366',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    IsiBiodata2:{
        fontSize: 16,
        paddingBottom: 8,
        color:'#3EC6FF',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    iconBox1: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    iconBox2: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 10,
    },
    textBox: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#003366'
    },
    navItem: {
        margin: 2,
        alignItems: 'center',
    },
    tabItem1: {
        alignItems:'center',
        justifyContent: 'center',
      },
    tabItem2: {
        paddingLeft:60,
        alignItems:'flex-start',
        justifyContent: 'center',
      },
    contact: {
        fontSize: 15,
        textAlign: 'center',

    },
    textKategori:{
        textAlign:'center',
        fontSize:10,
    },
})