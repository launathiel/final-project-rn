import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text ,View ,Image, TouchableOpacity, FlatList, } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import data from './skillData.json';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import BoxItem from '../Tugas14/box';

export default function App() {
  return (
    <View style={styles.container}>
        <View style={styles.LogoBar}>
            <Image source={require('../Tugas13/asset/logo2.png')} style={{width:150, height:40}}/>
        </View>
        <View style={{flexDirection: "column", marginLeft: -135, marginTop: -15}}>
            <View style={{flexDirection: "row"}}>
                <View style={styles.Photo}>
                    <Image source={require('../Tugas13/asset/avatar.png')} style={{width:50, height:50}}/>
                </View> 
                <View style={{flexDirection: "column"}}>
                    <Text>Hai,</Text> 
                    <Text>Laurentius Nathaniel</Text> 
                </View>
            </View>    
        </View>
        <View>
            <Text style={styles.PalingAtas}>SKILL</Text>
            <View style={{ borderBottomColor: '#003366', borderBottomWidth: 2, padding:2, borderBottomEndRadius:20,}}/>
        </View>
        <View style={styles.kategori}>
            <View style={{flexDirection:"row", alignItems: 'center',}}>
                <View style={styles.boxKategori}>
                    <Text style={styles.textKategori}>Library/Framework</Text>
                </View>
                <View style={styles.boxKategori}>
                    <Text style={styles.textKategori}>Bahasa Pemrograman</Text>
                </View>
                <View style={styles.boxKategori}>
                    <Text style={styles.textKategori}>Teknologi</Text>
                </View>
            </View>
        </View>
        <View style={styles.body}>
            <FlatList
            data={data.items}
            renderItem={(box)=><BoxItem box={box.item}/>}
            keyExtractor={(item)=>item.id}
            />
        </View>
        
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    LogoBar:{
        backgroundColor:'white',
        marginTop:0,
        marginLeft:210,
        marginBottom:10,
    },
    PalingAtas: {
        fontSize: 30,
        paddingBottom: 4,
        color:'#003366',
        fontWeight: 'bold',
    },
    Photo: {
        marginTop:0,
        marginRight: 10,
        marginBottom:5,
    },
    boxx: {
        margin:2,
    },
    boxKategori: {
        width:100,
        height:30,
        padding: 4,
        backgroundColor: '#B4E9FF',
        margin: 5,
        borderRadius:8,
        alignItems:'center',
        justifyContent:'center',
    },

    body:{
        flex:1,
        marginBottom:8,
    },
    
    box2: {
        width:294,
        height:100,
        padding: 5,
        backgroundColor: '#EFEFEF',
        margin: 15,
        borderRadius:10,
    },
    Biodata: {
        marginBottom: 6,
    },
    IsiBiodata1:{
        fontSize: 24,
        paddingBottom: 8,
        color:'#003366',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    IsiBiodata2:{
        fontSize: 16,
        paddingBottom: 8,
        color:'#3EC6FF',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    iconBox1: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 10,
    },
    iconBox2: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingTop: 10,
    },
    textBox: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: 16,
        color: '#003366'
    },
    navItem: {
        margin: 2,
        alignItems: 'center',
    },
    tabItem1: {
        alignItems:'center',
        justifyContent: 'center',
      },
    tabItem2: {
        paddingLeft:60,
        alignItems:'flex-start',
        justifyContent: 'center',
      },
    contact: {
        fontSize: 15,
        textAlign: 'center',

    },
    textKategori:{
        textAlign:'center',
        fontSize:10,
    },
})