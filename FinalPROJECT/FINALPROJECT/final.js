import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import login from './src/login';
import register from './src/register';
import home from './src/home';
import detail from './src/detail';
import about from './src/about';
import LoginScreen from './src/login';

const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

const iconTab = ({ route }) => {
  return ({
      tabBarIcon: ({ focused, color, size }) => {
          if (route.name == 'home') {
              return <FontAwesome5 name={'home'} size={size} color={color} solid />
          } else if (route.name == 'detail') {
              return <FontAwesome5 name={'plug'} size={size} color={color} solid />
          } else if (route.name == 'about') {
              return <FontAwesome5 name={'user-circle'} size={size} color={color} solid />
          }
      },
  });
}

const TabsStackScreen = () => (
  <TabsStack.Navigator screenOptions={iconTab} >
        <TabsStack.Screen name='home' component={home}
          options={{
              title: 'Home'
          }} />
        <TabsStack.Screen name='detail' component={detail}
          options={{
              title: 'Details'
          }} />
        <TabsStack.Screen name='about' component={about}
          options={{
              title: 'About'
          }}
      />
  </TabsStack.Navigator>
);

const DrawerStackScreen = () => (
  <DrawerStack.Navigator >
      <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}
          options={{
              title: 'Home',
          }} />
      <DrawerStack.Screen name='AboutScreen' component={about}
          options={{
              title: 'About'
          }}
      />
      <DrawerStack.Screen name='Logout' component={LoginScreen}
          options={{
              title: 'Logout'
          }}
      />
  </DrawerStack.Navigator>
);

const RootStackScreen = () => (
  <RootStack.Navigator  
    screenOptions={{
    headerStyle: {
      backgroundColor: '#41729f',
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }}>
      <RootStack.Screen name='LoginScreen' component={login}
          options={{
              title: 'Sign In'
          }}
      />
      <RootStack.Screen name='RegisterScreen' component={register}
          options={{
              title: 'Register'
          }}
      />
      <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen}
          options={{
              title: 'Home'
          }}
      />
  </RootStack.Navigator>
);

export default () => (
  <NavigationContainer>
      <RootStackScreen />
  </NavigationContainer>
);